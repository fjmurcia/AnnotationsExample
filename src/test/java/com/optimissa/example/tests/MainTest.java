package com.optimissa.example.tests;

import com.optimissa.example.beans.Pedido;
import com.optimissa.example.main.ReportePedidos;
import com.optimissa.example.main.TiendaVirtual;
import java.util.List;
import static junit.framework.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author XE56445
 */
public class MainTest {

    /**
     * Comprueba que la Tienda devuelve un numero correcto de pedidos.
     */
    @Test
    public void checkGetListaPedidos() {        
        int numeroPedidos = 10;
        List<Pedido> pedidos = TiendaVirtual.getListaPedidos(numeroPedidos);
        // assert statements
        assertEquals("Should be 10 pedidos: ", numeroPedidos, pedidos.size());
    }
    
    @Test
    public void checkReporte()
    {
        int numeroPedidos = 10;
        List<Pedido> pedidos = TiendaVirtual.getListaPedidos(numeroPedidos);
        ReportePedidos.getReportePedidos(pedidos);        
    }
}
