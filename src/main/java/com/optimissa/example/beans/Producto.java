package com.optimissa.example.beans;

import com.optimissa.example.annotations.Cifrado;
import com.optimissa.example.annotations.Mayusculas;

/**
 *
 * @author XE56445
 */
public class Producto {
    
    private Integer id;    
    private String nombre;
    @Cifrado(indice =2)
    @Mayusculas    
    private String marca;

    public Producto(Integer id, String nombre, String marca) {
        this.id = id;
        this.nombre = nombre;
        this.marca = marca;
    }

    
    
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }
    
    
    
   
    
}
