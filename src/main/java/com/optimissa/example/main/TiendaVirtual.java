package com.optimissa.example.main;

import com.optimissa.example.beans.Cliente;
import com.optimissa.example.beans.Pedido;
import com.optimissa.example.beans.Producto;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author XE56445
 */
public class TiendaVirtual {
    
    private static final List<Cliente> listaClientes = new ArrayList<>();
    private static final List<Producto> listaProductos = new ArrayList<>();
    
    static
    {
        
        listaClientes.add(new Cliente(1001,"Francisco","Murcia","625445588","francisco@optimissa.com"));
        listaClientes.add(new Cliente(2020,"Andres","Pujol","689887755","andres@optimissa.com"));
        listaClientes.add(new Cliente(3254,"Alfonso","Sanz","623568855","alfonsanz@optimissa.com"));
        listaClientes.add(new Cliente(4557,"Natalia","Casas","685778899","nataliacasas@optimissa.com"));
        
        listaProductos.add(new Producto(87,"iPhone6","Apple"));
        listaProductos.add(new Producto(52,"GalaxyS7","Samsung"));
        listaProductos.add(new Producto(58,"GalaxyAce","Samsung"));
        listaProductos.add(new Producto(77,"TabletInfame","HTC"));
        listaProductos.add(new Producto(78,"AquaSphera+3","BQ"));
        listaProductos.add(new Producto(50,"RobotAspirador","Roomba"));
        
    }
    
    /**
     * Metodo de ayuda para realizar los pedidos
     * 
     * @param n numero de items en el pedido
     * @return lista de pedidos
     */
    public static List<Pedido> getListaPedidos(int n)
    {
        Random r = new Random();
        List<Pedido> toReturn = new ArrayList<>();
        
        for(int i=0;i<n;i++)
        {
            Cliente cliente = listaClientes.get(r.nextInt(listaClientes.size()));
            Producto producto = listaProductos.get(r.nextInt(listaProductos.size()));
            toReturn.add(new Pedido(cliente, producto));            
        }
        return toReturn;
    }
    
}
