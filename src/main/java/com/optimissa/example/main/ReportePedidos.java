package com.optimissa.example.main;

import com.optimissa.example.annotations.Cifrado;
import com.optimissa.example.annotations.Mayusculas;
import com.optimissa.example.beans.Cliente;
import com.optimissa.example.beans.Pedido;
import com.optimissa.example.beans.Producto;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author XE56445
 */
public class ReportePedidos {

    private static final Logger LOG = Logger.getLogger(ReportePedidos.class.getName());

    public static String getReportePedidos(List<Pedido> pedidos) {
        LOG.log(Level.INFO, "Total de pedidos: {0}", pedidos.size());
        String s = "";

        for (Pedido pedido : pedidos) {
            Cliente cliente = pedido.getCliente();
            Producto producto = pedido.getProducto();

            LOG.log(Level.INFO, "{0}{1}", new Object[]{getLine(cliente), getLine(producto)});
        }

        return "";
    }

    private static String getLine(Object object) {
        String toReturn = "";
        for (Field field : object.getClass().getDeclaredFields()) {
            field.setAccessible(Boolean.TRUE);
            try {
                if (field.getAnnotations().length > 0) {
                    String tmp ="";
                    if (field.isAnnotationPresent(Mayusculas.class)) {
                        tmp = (field.get(object) + "").toUpperCase();
                    }

                    if (field.isAnnotationPresent(Cifrado.class)) {
                        Cifrado cifrado = field.getAnnotation(Cifrado.class);
                        if("".equals(tmp))
                        {
                            tmp = CifradoCesar.cifrar((field.get(object) + ""), cifrado.indice());
                        }else
                        {
                            //already processed by other annotation
                            tmp = CifradoCesar.cifrar(tmp , cifrado.indice());
                        }                          
                    }
                    
                    toReturn = toReturn + tmp + ";";
                    
                } else {
                    toReturn = toReturn + (field.get(object) + "") + ";";
                }

            } catch (IllegalArgumentException | IllegalAccessException ex) {
                Logger.getLogger(ReportePedidos.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return toReturn;
    }

}
