package com.optimissa.example.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;

@Retention(value=RUNTIME)
@Target(value=ElementType.FIELD)
public @interface Cifrado {
    int indice();    
}
